# A collection of `.gitlab-ci.yml` templates and includes for Security Products
[![build status](https://gitlab.com/gitlab-org/security-products/ci-templates/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/ci-templates/commits/master) [![coverage report](https://gitlab.com/gitlab-org/security-products/ci-templates/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/ci-templates/commits/master)

This is Security Products collection of [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/) file templates,
to be used in conjunction with [GitLab CI](https://docs.gitlab.com/ee/ci/).

These are for internal usage only. If you're looking for template to include in your projects to activate the Security Features, please refer to [the documentation](https://docs.gitlab.com/ee/user/application_security/).

## Folder structure

The `includes-dev` folder holds yaml files for development process and tools of the Security Product team.
See [include feature documentation](https://docs.gitlab.com/ee/ci/yaml/#include).

## Contributing guidelines

Please see the [contribution guidelines](CONTRIBUTING.md)
